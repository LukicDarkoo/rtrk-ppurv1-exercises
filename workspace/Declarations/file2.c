#include <stdio.h>
#include <inttypes.h>
#include "file3.h"

uint_least16_t global_variable;

void use_it() {
	global_variable++;
	printf("%s  %s  global_variable=%d\n", __FILE__, __FUNCTION__ , global_variable);
}

