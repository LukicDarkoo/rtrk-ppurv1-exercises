#ifndef CircularBuffer_H_
#define CircularBuffer_H_

#include <stdio.h>
#include <assert.h>

#define BUFFER_SIZE 10

typedef int buffer_type;

typedef struct _CircularBuffer {
	buffer_type data[BUFFER_SIZE];
	buffer_type* rear;
	buffer_type* front;
} CircularBuffer;

int CircularInit(CircularBuffer* buffer);
int CircularIsFull(CircularBuffer* buffer);
int CircularIsEmpty(CircularBuffer* buffer);
int CircularPut(CircularBuffer* buffer, buffer_type element);
buffer_type CircularGet(CircularBuffer* buffer);
int CircularEmptyBuff(CircularBuffer* buffer);
int CircularDump(CircularBuffer* buffer);

#endif
