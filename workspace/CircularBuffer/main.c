#include "CircularBuffer.h"
#include <stdlib.h>

int main() {
	int i;
	CircularBuffer buffer;

	CircularInit(&buffer);

	// Add elements to buffer
	for (i = 0; i < 9; i++) {
		CircularPut(&buffer, i);
	}

	CircularGet(&buffer);
	CircularPut(&buffer, 10);

	printf("CircularIsFull: %d\n", CircularIsFull(&buffer));

	CircularDump(&buffer);

	return 0;
}
