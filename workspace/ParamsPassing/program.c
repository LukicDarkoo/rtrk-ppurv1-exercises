#include <stdio.h>
#include <stdint.h>

typedef struct array {
	int_least32_t* values;
	int_least16_t* indexes;
	int n;
} Array;

static const int treshold = 20;

static int_least32_t values[4000];
static int_least16_t indexes[4000];

static void sort(int_least32_t* values, int_least16_t* indexes, int_least16_t n)
{
	int_least16_t i;
	int_least16_t j;
	for (i = 0; i < (n - 1); i++)
	{
		for (j = i + 1; j < n; j++)
		{
			if (values[i] < values[j])
			{
				int_least32_t tmp;
				int_least16_t tmp1;

				tmp = values[i];
				values[i] = values[j];
				values[j] = tmp;

				tmp1 = indexes[i];
				indexes[i] = indexes[j];
				indexes[j] = tmp1;
			}
		}
	}
}


static void print(
		int_least32_t* values,
		int_least16_t* indexes,
		int_least16_t n,
		int_least32_t* values_out,
		int_least16_t* indexes_out,
		int_least16_t* n_out)
{
	int_least16_t i = 0;
	int_least16_t ni = 0;
	printf("Up to the first treshold: ");
	while (values[i] > treshold && i < n)
	{
		if (values[i] % 2 == 0) {
			values_out[ni] = values[i];
			indexes_out[ni] = indexes[i];
			ni++;
		}

		printf("%d(%d) ", values[i], indexes[i]);
		i++;
	}
	printf("\n");

	*n_out = ni;
}


void main()
{
	int_least16_t i;
	int_least16_t n;
	int_least16_t ni;

	int_least32_t values_out[4000];
	int_least16_t indexes_out[4000];

	printf("Number of elements: ");
	scanf("%d", &n);
	printf("\n");
	for (i = 0; i < n; i++)
	{
		printf("%d. element: ", i);
		scanf("%d", &values[i]);
		indexes[i] = i;
		printf("\n");
	}

	sort(values, indexes, n);

	print(values, indexes, n, values_out, indexes_out, &ni);

	printf("\n\n");
	for (i = 0; i < ni; i++) {
		printf("%d ", values_out[i]);
	}
}
