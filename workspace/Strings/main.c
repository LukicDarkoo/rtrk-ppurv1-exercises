#include <stdio.h>

#define SUCCESS 0
#define ERROR 1
#define STRING " Darkoo  "

int stringLength(char str[]) {
	int length = 0;
	while (str[length] != '\0') {
		length++;
	}
	return length;
}

void stringTrim(char str[]) {
	int spacesAtBegining = 0;
	int tempStringLength;
	int i;

	// Remove spaces from the end
	i = stringLength(str) - 1;
	while (i >= 0 && str[i] == ' ') {
		str[i] = '\0';
		i--;
	}

	// Count spaces at the beginning & delete them
	while (str[spacesAtBegining++] == " ");
	tempStringLength = stringLength(str);
	for (i = 0; i < tempStringLength; i++) {
		str[i] = str[i + spacesAtBegining];
		str[i + spacesAtBegining] = '\0';
	}
}

int extractSubstring(const char *inStr, char * outStr, int beginning, int end) {
	int i;

	// Validate
	if (end > stringLength(outStr)) {
		return ERROR;
	}

	// Make substring
	for (i = beginning; i < end; i++) {
		outStr[i - beginning] = inStr[i];
	}
	outStr[end] = '\0';

	return SUCCESS;
}

int concateStrings(char * str1, const char * str2) {
	int i;
	int j;
	int leftStringLength = stringLength(str1);

	for (j = 0, i = leftStringLength; j < stringLength((char *)str2); i++, j++) {
		str1[i] = str2[j];
	}

	return SUCCESS;
}

int main() {
	char str[] = STRING;

	// stringLength(char str[]) test
	printf("Length of \"%s\" is %d\n", STRING, stringLength(STRING));

	// stringTrim(char str[]) test
	stringTrim(str);
	printf("Trimmed value of \"%s\" is \"%s\"\n", STRING, str);

	// extractSubstring(const char *inStr, char * outStr, int beginning, int end) test
	extractSubstring(STRING, str, 1, 5);
	printf("Substring value of \"%s\" is \"%s\"\n", STRING, str);

	// concateStrings(char * str1, const char * str2) test
	concateStrings(str, "Darko");
	printf("Contacted strings \"%s\" and \"%s\ are \"%s\"\n", "Darko", "Darko", str);


	return 0;
}
