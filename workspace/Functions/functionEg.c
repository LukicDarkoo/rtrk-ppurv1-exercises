#include "functionEg.h"


long _factorial(int n)
{
	if (n <= 1) return n;
	return n * factorial(n - 1);
}


long factorial(int n)
{
	int i = n;
	long result = 1;
	while (i > 1) {
		result = result * i;
		i--;
	}
	return result;
}

long fibonacci(long n)
{
	if (n <= 1) return n;
	return fibonacci(n - 1) + fibonacci(n - 2);
}


long _fibonacci(long n)
{
	long previous = 0;
	long result = 1;
	long temp;
	long i;

	for (i = 1; i < n; i++) {
		temp = result;
		result += previous;
		previous = temp;
	}

	return result;
}


int staticInFunction()
{
	static int i = 0;
	i++;
	{
		int j = 0;
		j = 5%i;
		i += j;

	}
	return i;
}
