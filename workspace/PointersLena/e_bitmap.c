/*
 ============================================================================
 Name        : e_bitmap.c
 Author      : rt-rk
 Version     :
 Copyright   : Your copyright notice
 Description : image processing example
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "bmp.h"
#include "imageProcessing.h"

#define __ARG_NUM__ 2

int main(int argc, char* argv[])
{
	char* path = "lena.bmp";
	int err = 0;
	BITMAPFILEHEADER bitmapFileHeader;
	BITMAPINFOHEADER bitmapInfoHeader;
	unsigned char* bitmapData;

	// Calculate averages
	printf("Calculate averages\n");
	bitmapData = LoadBitmapFile(path,&bitmapFileHeader, &bitmapInfoHeader);
	calculateMeanColors(bitmapData, bitmapInfoHeader.biWidth, bitmapInfoHeader.biHeight);

	// Mean filter
	printf("Mean filter\n");
	unsigned char* oData = (unsigned char*)malloc(sizeof(unsigned char) * bitmapInfoHeader.biWidth * bitmapInfoHeader.biHeight * 3);
	bitmapData = LoadBitmapFile(path,&bitmapFileHeader, &bitmapInfoHeader);
	applyMeanFilter(bitmapData, oData, bitmapInfoHeader.biWidth, bitmapInfoHeader.biHeight);
	SaveBitmapFile("out_mean.bmp", &bitmapFileHeader, &bitmapInfoHeader, oData);
	free(oData);

	// Apply image brightness to image
	printf("Applying brightness\n");
	bitmapData = LoadBitmapFile(path,&bitmapFileHeader, &bitmapInfoHeader);
	adjustImageBrightness(bitmapData, bitmapInfoHeader.biWidth,
		bitmapInfoHeader.biHeight, false, 70);
	err = SaveBitmapFile("out_brightness.bmp", &bitmapFileHeader,
		&bitmapInfoHeader, bitmapData);
	if (err) 
	{
		printf("Error trying to save bitmap\n");
	}
	free(bitmapData);

	printf("Applying dark image\n");
	bitmapData = LoadBitmapFile(path, &bitmapFileHeader, &bitmapInfoHeader);
	darkenImage(bitmapData, bitmapInfoHeader.biWidth,
		bitmapInfoHeader.biHeight);
	err = SaveBitmapFile("out_dark.bmp", &bitmapFileHeader,
		&bitmapInfoHeader, bitmapData);
	if (err)
	{
		printf("Error trying to save bitmap\n");
	}
	free(bitmapData);

	printf("Applying effect\n");
	bitmapData = LoadBitmapFile(argv[1], &bitmapFileHeader, &bitmapInfoHeader);
	effect(bitmapData, bitmapInfoHeader.biWidth,
		bitmapInfoHeader.biHeight);
	err = SaveBitmapFile("out_bw.bmp", &bitmapFileHeader,
		&bitmapInfoHeader, bitmapData);
	if (err) 
	{
		printf("Error trying to save bitmap\n");
	}
	free(bitmapData);

	return EXIT_SUCCESS;
}
