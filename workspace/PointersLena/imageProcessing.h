#ifndef IMAGEPROCESSING_H_
#define IMAGEPROCESSING_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

void darkenImage(unsigned char* imageData, int imageWidth, int imageHeight);

void applyMeanFilter(unsigned char* iData, unsigned char* oData, int iWidth, int iHeight);

void adjustImageBrightness(unsigned char* iData, int iWidth, int iHeight,
		bool brighten, unsigned char adjustmentValue);

void effect(unsigned char* iData, int iWidth, int iHeight);

void calculateMeanColors(unsigned char* iData, int iWidth, int iHeight);

#endif /* IMAGEPROCESSING_H_ */
