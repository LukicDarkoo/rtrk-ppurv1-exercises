
#include "imageProcessing.h"
#include "bmp.h"

#define BW_TRESHOLD 128
const unsigned char darkenFactor = 192;
const int brightnessMinimum = 20;
const int brightnessMaximum = 220;

#define KERNEL_SIZE 9
const int kernelLimit = KERNEL_SIZE / 2;

int calculateMean(unsigned char* iData, int w, int h, int iWidth, int iHeight) {
	int hi;
	int wi;
	int sum = 0;

	for (hi = -kernelLimit; hi <= kernelLimit; hi++) {
		for (wi = -kernelLimit; wi <= kernelLimit; wi++) {
			sum += iData[3 * (h + hi) * iWidth + 3 * (w + wi)];
		}
	}

	return (int)(sum / (float)(KERNEL_SIZE * KERNEL_SIZE));
}

void applyMeanFilter(unsigned char* iData, unsigned char* oData, int iWidth, int iHeight) {
	int hi;
	int wi;
	int color;

	for (hi = kernelLimit; hi < iHeight - kernelLimit; hi++) {
		for (wi = kernelLimit; wi < iWidth - kernelLimit; wi++) {
			for (color = 0; color < 3; color++) {
				oData[3 * hi * iWidth + 3 * wi + color] = calculateMean(iData + color, wi, hi, iWidth, iHeight);
			}
		}
	}
}

void calculateMeanColors(unsigned char* iData, int iWidth, int iHeight) {
	int hi;
	int wi;

	unsigned long long sumR = 0;
	unsigned long long sumG = 0;
	unsigned long long sumB = 0;

	long long sumPixels = iWidth * iHeight;

	for (hi = 0; hi < iHeight; hi++) {
		for (wi = 0; wi < iWidth; wi++) {
			sumR += iData[3 * hi * iWidth + 3 * wi];
			sumG += iData[3 * hi * iWidth + 3 * wi + 1];
			sumB += iData[3 * hi * iWidth + 3 * wi + 2];
		}
	}

	printf("Average: R = %f, G = %f, B = %f\n",
			sumR / (float)sumPixels,
			sumG / (float)sumPixels,
			sumB / (float)sumPixels
	);
}

void darkenImage(unsigned char* iData, int iWidth, int iHeight)
{
	int i;
	int j;
	unsigned char* ptr = iData;
	
	for (i=0; i<iHeight; i++)
	{
		for (j=0; j<iWidth; j++)
		{
			*ptr = *ptr + darkenFactor;
			ptr++;
			*ptr = *ptr + darkenFactor;
			ptr += 1;
			*ptr = *ptr + darkenFactor;
			ptr += 1;
		}
	}
}

void adjustImageBrightness(unsigned char* iData, int iWidth, int iHeight,
		bool brighten, unsigned char adjustmentValue)
{
	int i;
	int newValue;

	for (i=0; i<iWidth*iHeight*3; i++)
	{
		newValue = iData[i];
		if(brighten)
		{
			newValue += adjustmentValue;
		}
		else
		{
			newValue -= adjustmentValue;
		}

		if(newValue < brightnessMinimum)
		{
			newValue = brightnessMinimum;
		}
		else if(newValue > brightnessMaximum)
		{
			newValue = brightnessMaximum;
		}

		iData[i] = newValue;
	}
}

void effect(unsigned char* iData, int iWidth, int iHeight)
{
	unsigned char* ptr = iData;
	int i;
	int j;
	int k;
	
	for (i=0; i<iHeight; i++)
	{
		for (j=0; j<iWidth; j++)
		{
			for (k=0; k<3; k++)
			{
				if(*(ptr+k) > BW_TRESHOLD)
				{
					*(ptr+k) = 255;
				}
				else
				{
					*(ptr+k) = 0;
				}
			}
			ptr += 3;
		}
	}
}
