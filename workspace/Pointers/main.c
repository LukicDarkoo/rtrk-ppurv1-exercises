#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>


static const int_least32_t treshold1 = 1;
static const int_least32_t treshold2 = 2;
static const int_least32_t treshold3 = 3;


typedef struct _s
{
	int_least32_t value;
	int_least16_t index;
} val_ind_struct_t;

static val_ind_struct_t struct_array[4000];


static void bubble_sort(val_ind_struct_t* str_array, int_least16_t n)
{
	int_least16_t i;
	int_least16_t j;
	for (i = 0; i < (n - 1); i++)
	{
		for (j = i + 1; j < n; j++)
		{
			if (str_array[i].value < str_array[j].value)
			{
				val_ind_struct_t tmp;

				tmp = str_array[i];
				str_array[i] = str_array[j];
				str_array[j] = tmp;
			}
		}
	}
}

int compar(const void* left, const void* right) {
	val_ind_struct_t* real_left = (val_ind_struct_t *)(left);
	val_ind_struct_t* real_right = (val_ind_struct_t *)(right);

	printf("compar %d - %d = %d\n", real_left->value, real_right->value, real_left->value - real_right->value);

	if (real_left->value < real_right->value) {
		return 1;
	} else if (real_left->value > real_right->value) {
		return -1;
	}
	return 0;
}

static void quick_sort(val_ind_struct_t* str_array, int_least16_t n)
{
	qsort((void *)str_array, (size_t)n, sizeof(val_ind_struct_t), compar);
}




static void print(const val_ind_struct_t* str_array, int_least16_t n, int_fast8_t tr_index)
{
	int_least16_t i = 0;
	int_least32_t treshold;
	printf("Up to the %d. treshold: ", tr_index);

	switch (tr_index)
	{
	case 1:
		treshold = treshold1;
		break;
	case 2:
		treshold = treshold2;
		break;
	case 3:
		treshold = treshold3;
		break;
	}

	while (str_array[i].value > treshold && i < n)
	{
		printf("%d(%d) ", str_array[i].value, str_array[i].index);
		i++;
	}
	printf("\n");
}


void main()
{
	void (*sort_functions[2])(val_ind_struct_t* str_array, int_least16_t n) = { bubble_sort, quick_sort };
	int_least16_t i;
	int_least16_t n;
	int_fast8_t treshold_index;
	int_fast8_t sort_index;
	printf("Number of elements: ");
	scanf("%d", &n);
	printf("\n");
	for (i = 0; i < n; i++)
	{
		printf("%d. element: ", i);
		scanf("%d", &(struct_array[i].value));
		struct_array[i].index = i;
		printf("\n");
	}
	printf("Trashold you want to use (1, 2, or 3): ");
	scanf("%d", &treshold_index);
	printf("\n");
	printf("Sorting algorithm you want to use (1- bubble, 2- quick): ");
	scanf("%d", &sort_index);
	printf("\n");

	sort_functions[sort_index](struct_array, n);

	print(struct_array, n, treshold_index);
}
